package models

type Response struct {
	IP      string `json:"ip"`
	City    string `json:"city"`
	Country string `json:"country"`
}
