package ip

import (
	"math"
	"sync/atomic"
)

var (
	currentIP = uint32(math.MaxInt32)
)

func Random() string {
	var ip = atomic.AddUint32(&currentIP, 1)

	return uint32ToString(ip)
}

func uint32ToString(ip uint32) string {
	var result = make([]byte, 0, 15)

	result = b2s(result, byte(ip>>24))
	result = append(result, '.')
	result = b2s(result, byte(ip>>16))
	result = append(result, '.')
	result = b2s(result, byte(ip>>8))
	result = append(result, '.')
	result = b2s(result, byte(ip))

	return string(result)
}

func b2s(dst []byte, b byte) []byte {
	if b == 0 {
		return append(dst, '0')
	}

	if b > 100 {
		dst = append(dst, b/100+'0')

		b %= 100
	}

	if b > 10 {
		dst = append(dst, b/10+'0')

		b %= 10
	}

	if b > 0 {
		dst = append(dst, b+'0')
	}

	return dst
}
