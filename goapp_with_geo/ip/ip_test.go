package ip

import (
	"github.com/stretchr/testify/require"
	"math"
	"testing"
)

func TestUint32ToString(t *testing.T) {
	require.Equal(t, "0.0.0.0", uint32ToString(0))
	require.Equal(t, "0.0.0.1", uint32ToString(1))
	require.Equal(t, "0.0.1.1", uint32ToString(1<<8+1))
	require.Equal(t, "0.0.2.1", uint32ToString(2<<8+1))
	require.Equal(t, "0.3.2.1", uint32ToString(3<<16+2<<8+1))
	require.Equal(t, "4.3.2.1", uint32ToString(4<<24+3<<16+2<<8+1))
	require.Equal(t, "255.255.255.255", uint32ToString(math.MaxUint32))
}

func BenchmarkUint32ToString(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			uint32ToString(math.MaxUint32)
		}
	})
}
