package main

import (
	"encoding/json"
	"github.com/oschwald/geoip2-golang"
	"github.com/valyala/fasthttp"
	"gitlab.com/go-yp/nginx-geo-module-research/goapp_with_geo/geo"
	"gitlab.com/go-yp/nginx-geo-module-research/goapp_with_geo/ip"
	"gitlab.com/go-yp/nginx-geo-module-research/models"
	"log"
)

func main() {
	log.Println("goapp-with-geo start on port = 9081")

	var err = geo.Context(func(geoReader *geoip2.Reader) error {
		var geoService = geo.NewService(geoReader)

		var server = &fasthttp.Server{
			Handler: func(ctx *fasthttp.RequestCtx) {
				var randomIP = ip.Random()

				var city, county, err = geoService.City(randomIP)
				if err != nil {
					ctx.Response.SetStatusCode(fasthttp.StatusNotFound)

					log.Printf("cannot find geo for ip = %s by err = %+v", randomIP, err)

					return
				}

				var marshalErr = json.NewEncoder(ctx).Encode(models.Response{
					IP:      randomIP,
					City:    city,
					Country: county,
				})

				if marshalErr != nil {
					ctx.Response.SetStatusCode(fasthttp.StatusNotFound)

					log.Printf("cannot json marshal by err = %+v", marshalErr)

					return
				}
			},
			ReadBufferSize:     3 * 4096,
			MaxRequestBodySize: 25 * 1024 * 1024,
		}

		return server.ListenAndServe(":9081")
	})

	if err != nil {
		log.Printf("goapp-with-geo stop with %+v\n", err)
	}
}
