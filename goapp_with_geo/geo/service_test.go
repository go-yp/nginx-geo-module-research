package geo

import (
	"github.com/oschwald/geoip2-golang"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestService_City(t *testing.T) {
	var err = Context(func(geoReader *geoip2.Reader) error {
		var service = NewService(geoReader)

		{
			var city, country, err = service.City("")
			require.Equal(t, "", city)
			require.Equal(t, "", country)
			require.Equal(t, ErrRequiredIP, err)
		}

		{
			var city, country, err = service.City("188.239.25.25")
			require.Equal(t, "UA", country)
			require.Equal(t, "Kyiv", city)
			require.NoError(t, err)
		}

		return nil
	})

	require.NoError(t, err)
}

func BenchmarkService_City(b *testing.B) {
	var err = Context(func(geoReader *geoip2.Reader) error {
		var service = NewService(geoReader)

		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				var city, country, err = service.City("188.239.25.25")

				require.Equal(b, "UA", country)
				require.Equal(b, "Kyiv", city)
				require.NoError(b, err)

			}
		})

		return nil
	})

	require.NoError(b, err)
}
