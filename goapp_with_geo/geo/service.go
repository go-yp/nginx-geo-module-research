package geo

import (
	"errors"
	"fmt"
	"github.com/oschwald/geoip2-golang"
	"net"
)

var (
	ErrRequiredIP = errors.New("ip required")
	ErrInvalidIP  = errors.New("ip invalid")
)

type Service struct {
	geoReader *geoip2.Reader
}

func NewService(geoReader *geoip2.Reader) *Service {
	return &Service{geoReader: geoReader}
}

func (s *Service) City(ip string) (string, string, error) {
	if ip == "" {
		return "", "", ErrRequiredIP
	}

	var parsedIP = net.ParseIP(ip)
	if parsedIP == nil {
		return "", "", ErrInvalidIP
	}

	var record, err = s.geoReader.City(parsedIP)
	if err != nil {
		return "", "", fmt.Errorf("try geoip2 city with %w", err)
	}

	return record.City.Names["en"], record.Country.IsoCode, nil
}
