package geo

import (
	"github.com/oschwald/geoip2-golang"
)

func Context(handler func(geoReader *geoip2.Reader) error) error {
	geoReader, err := geoip2.Open("/var/go/src/resources/GeoIP2-City.mmdb")
	if err != nil {
		return err
	}
	defer geoReader.Close()

	return handler(geoReader)
}
