wrk-nginx-goapp-with-geo:
	wrk -t16 -c512 http://localhost:8081/

wrk-nginx-geo-with-goapp:
	wrk -t16 -c512 http://localhost:8082/

up:
	sudo docker-compose up -d

build:
	sudo docker-compose up -d --build

app:
	sudo docker exec -it goapp_with_geo sh

run:
	sudo docker exec goapp_with_geo go run ./nginx-geo-go/main.go

down:
	sudo docker-compose down

nginx-reload:
	sudo docker exec nginx nginx -s reload
	sudo docker exec nginx_with_geo nginx -s reload

nginx-error-log:
	sudo docker exec nginx less /var/log/nginx/error.log

mmdb:
	sudo rm -rf /var/go/src/resources
	sudo mkdir -p /var/go/src/resources
	sudo ln -s ~/go/src/gitlab.com/go-yp/nginx-geo-module-research/resources/GeoIP2-City.mmdb /var/go/src/resources/GeoIP2-City.mmdb