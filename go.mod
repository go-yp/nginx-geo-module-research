module gitlab.com/go-yp/nginx-geo-module-research

go 1.14

require (
	github.com/oschwald/geoip2-golang v1.4.0
	github.com/stretchr/testify v1.4.0
	github.com/valyala/fasthttp v1.12.0
)
