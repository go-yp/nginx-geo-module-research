# nginx-geo-module-research

### Source
```bash
wrk -t16 -c512 http://localhost:8081/
```

```text
Running 10s test @ http://localhost:8082/
  16 threads and 512 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     2.11ms    4.03ms  68.59ms   90.16%
    Req/Sec    31.03k     6.38k  120.04k    78.48%
  4962561 requests in 10.09s, 643.64MB read
Requests/sec: 491638.53
Transfer/sec:     63.77MB
```

### (Nginx) with (Go app)
```bash
wrk -t16 -c512 http://localhost:8081/
```

```text
Running 10s test @ http://localhost:8081/
  16 threads and 512 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   165.88ms  290.78ms   1.41s    82.73%
    Req/Sec     2.09k   776.11     8.35k    79.99%
  323270 requests in 10.10s, 53.01MB read
  Socket errors: connect 0, read 0, write 0, timeout 15
Requests/sec:  32006.79
Transfer/sec:      5.25MB
```

### (Nginx) with (Go app with geo)
```bash
wrk -t16 -c512 http://localhost:8081/
```

```text
Running 10s test @ http://localhost:8081/
  16 threads and 512 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   192.43ms  305.27ms   1.24s    81.11%
    Req/Sec     1.96k   733.29     8.83k    74.06%
  310537 requests in 10.07s, 59.70MB read
  Socket errors: connect 0, read 0, write 0, timeout 24
  Non-2xx or 3xx responses: 72892
Requests/sec:  30837.26
Transfer/sec:      5.93MB
```

### Development
* [wrk documentation](https://github.com/wg/wrk)
* [ngx_http_geoip_module](http://nginx.org/en/docs/http/ngx_http_geoip_module.html)
* [ngx_http_geoip2_module](ngx_http_geoip2_module)
* [GeoIP.dat.gz and GeoLiteCity.dat.gz](https://mirrors-cdn.liferay.com/geolite.maxmind.com/download/geoip/database/)

### Resources:
* [github.com/openbridge/nginx](https://github.com/openbridge/nginx/blob/master/Dockerfile)
* [Use Nginx Reverse Proxy to serve Go with Docker](https://medium.com/@alessandromarinoac/docker-nginx-golang-reverse-proxy-d8244778bd43)
* [NGINX Tuning For Best Performance](https://github.com/denji/nginx-tuning)