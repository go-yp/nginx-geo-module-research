package main

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	"gitlab.com/go-yp/nginx-geo-module-research/models"
	"log"
)

func main() {
	log.Println("goapp start on port = 9082")

	var server = &fasthttp.Server{
		Handler: func(ctx *fasthttp.RequestCtx) {
			var (
				ip     = string(ctx.Request.Header.Peek("X-RESEARCH-IP"))
				city   = string(ctx.Request.Header.Peek("X-RESEARCH-CITY"))
				county = string(ctx.Request.Header.Peek("X-RESEARCH-COUNTRY"))
			)

			var marshalErr = json.NewEncoder(ctx).Encode(models.Response{
				IP:      ip,
				City:    city,
				Country: county,
			})

			if marshalErr != nil {
				ctx.Response.SetStatusCode(fasthttp.StatusNotFound)

				log.Printf("cannot json marshal by err = %+v", marshalErr)

				return
			}
		},
		ReadBufferSize:     3 * 4096,
		MaxRequestBodySize: 25 * 1024 * 1024,
	}

	var err = server.ListenAndServe(":9082")

	if err != nil {
		log.Printf("goapp stop with %+v\n", err)
	}
}
